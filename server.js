const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const encrypt = require('./encrypt');

const cors = require('cors');
app.use(cors());
app.options('*', cors());

app.get('/', (req, res) => {
    res.json({"message": "Server Working!!"});
});

app.get('/encryptionKey/:apiKey', (req, res) => {
    let apiKey = req.params.apiKey;
    let encyptionKey = encrypt.chilkatExample(apiKey);
    console.log(encyptionKey);
    res.send(encyptionKey);
    // let encryptionObject = encrypt.getEncryptionKey(apiKey);
    // console.log(encryptionObject);
    // res.send(encryptionObject);
})

// listen for requests at port 3000
app.listen(3000, () => {
    console.log("Server is listening on port 3000");
});