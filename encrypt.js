const os = require('os');
const crypto = require("crypto");
let chilkat;
if (os.platform() == 'win32') {
    if (os.arch() == 'ia32') {
        chilkat = require('@chilkat/ck-node14-win-ia32');
    } else {
        chilkat = require('@chilkat/ck-node14-win64');
    }
} else if (os.platform() == 'linux') {
    if (os.arch() == 'arm') {
        chilkat = require('@chilkat/ck-node14-arm');
    } else if (os.arch() == 'x86') {
        chilkat = require('@chilkat/ck-node14-linux32');
    } else {
        chilkat = require('@chilkat/ck-node14-linux64');
    }
} else if (os.platform() == 'darwin') {
    chilkat = require('@chilkat/ck-node14-macosx');
}

exports.chilkatExample = (apiKey) => {

    let crypt = new chilkat.Crypt2();
    crypt.CryptAlgorithm = "aes";
    crypt.KeyLength = 256;

    let prng = new chilkat.Prng();

    let jwkAes = new chilkat.JsonObject();
    jwkAes.EmitCompact = false;

    jwkAes.AppendString("kty","oct");
    jwkAes.AppendString("kid","2");
    jwkAes.AppendString("k",prng.GenRandom(32,"base64url"));
    return jwkAes.Emit();
}


exports.getEncryptionKey = (apiKey) => {
    // const algorithm = 'aes-256-cbc';
    const initVector = crypto.randomBytes(16);
    const securityKey = crypto.randomBytes(32);
    return {iv: initVector, key: securityKey};
}