##Steps to set up Node Server

#Install packages
Move to project folder and run `npm install` in command prompt.

#Run server
Move to project folder and run `npm start` in command prompt. This will start the server at `localhost:3000`.
